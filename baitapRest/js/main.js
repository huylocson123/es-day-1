function sum(...score) {
    let total = 0;
    score.forEach((sc) => total += sc)
    return total;
}

document.getElementById("btnKhoi1").addEventListener("click", function() {
    var toan = document.getElementById("inpToan").value * 1;
    var ly = document.getElementById("inpLy").value * 1;
    var hoa = document.getElementById("inpHoa").value * 1;
    document.getElementById("tbKhoi1").innerHTML = sum(toan, ly, hoa);
})
document.getElementById("btnKhoi2").addEventListener("click", function() {
    var van = document.getElementById("inpVan").value * 1;
    var su = document.getElementById("inpSu").value * 1;
    var dia = document.getElementById("inpDia").value * 1;
    var engLish = document.getElementById("inpEnglish").value * 1;
    document.getElementById("tbKhoi2").innerHTML = sum(van, su, dia, engLish);
})