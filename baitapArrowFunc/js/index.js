const listColor = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];
let renderColor = () => {
    for (let i = 0; i < listColor.length; i++) {
        let color = listColor[i];
        let btn = document.createElement("button");
        btn.className = 'color-button ' + color;
        btn.onclick = () => {
            document.querySelector("#house").className = 'house ' + color;
        }
        document.querySelector("#colorContainer").appendChild(btn);
    }
}
window.onload = () => {
    renderColor();
};